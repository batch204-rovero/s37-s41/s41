const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

//Route for checking if the user's email is already existing in our database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	/*
		id: '63401354f69e24abfb9bcaef',
		email: 'aronrovero@mail.com',
		isAdmin: false,
		iat: 1665404161,
		exp: 1665411361

	*/

	console.log(userData);
	console.log(req.headers.authorization)

	//Provides the user's ID for the getProfile controller method 
	userController.getProfile(userData).then(resultFromController => res.send(resultFromController));
});

//Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router;