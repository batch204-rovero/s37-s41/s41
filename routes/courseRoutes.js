const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require('../auth.js');

//Route for creating a course
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	//Mini-Activity
	if (userData.isAdmin) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		return false;
	}
});

//Route for retreiving all courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

//Retreiving all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

//Retreiving a specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

//Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	courseController.updateCourse(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
});

//Route for archiving a course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin) {
		courseController.archiveCourse(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
	} else {
		return false;
	}
});

module.exports = router;