const Course = require("../models/Course");
const User = require("../models/User");

//Create a new course
module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	});
};

//Retreive all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

//Retreiving all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

//Retreiving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	});
};

//Updating a course
module.exports.updateCourse = (reqParams, reqBody, data) => {
	return User.findById(data.id).then(result => {
		console.log(result)
		if(result.isAdmin === true) {
			// Specify the fields/properties of the document to be updated
			let updatedCourse = {
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			};

			// Syntax:
				// findByIdAndUpdate(document ID, updatesToBeApplied)
			return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

				// Course not updated
				if(error) {
					return false
				// Course updated successfully	
				} else {
					return true
				}
			})
		} else {
			return false
		}
	})
};


//Archiving a course
module.exports.archiveCourse = (reqParams, reqBody, data) => {
	if (data.isAdmin === true) {
		let archivedCourse = {
			isActive: reqBody.isActive
		}
		return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		});
	} else {
		return false
	}
};