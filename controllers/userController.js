const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Check if email exists
module.exports.checkEmailExist = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
};

//Controller for User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		//bcrypt.hashSync(<dataToBeHashed>, <saltRound>)
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	});
};

//User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			//bcrypt.compareSync(<dataToBeCompared>, <dataFromDB>)

			if (isPasswordCorrect) {
				console.log(result)
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	});
};

//Get user profile
module.exports.getProfile = (data) => {
	return User.findById(data.id).then(result => {
		console.log(result)
		result.password = "";
		return result;
	});
};

//Enroll user to class
module.exports.enroll = async (data) => {
	if (data.isAdmin === true) {
		return false
	} else {
		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.enrollments.push({courseId: data.courseId});
			return user.save().then((user, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			});
		});
		let isCourseUpdated = await Course.findById(data.courseId).then(course => {
			course.enrollees.push({userId: data.userId});
			return course.save().then((course, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			});
		});
		if (isUserUpdated && isCourseUpdated) {
			return true
		} else {
			return false
		}
	}
};