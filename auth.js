const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

//JSON Web Tokens

//Token Creation
module.exports.createAccessToken = (user) => {
	console.log(user)
	/*
		{
		  _id: new ObjectId("63401354f69e24abfb9bcaef"),
		  firstName: 'Aron',
		  lastName: 'Rovero',
		  email: 'aronrovero@mail.com',
		  password: '$2b$10$vIQwfDhVK/DCYF5/RzruGenReNV6M3EVgxeoVZWCx7/3YD4U2v/vO',
		  isAdmin: false,
		  mobileNo: '09171234567',
		  enrollments: [],
		  __v: 0
		}

	*/
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//Generate a token
		//jwt.sign(<payload>)
	return jwt.sign(data, secret, {expiresIn: "2h"})
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization
	//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzNDAxMzU0ZjY5ZTI0YWJmYjliY2FlZiIsImVtYWlsIjoiYXJvbnJvdmVyb0BtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NjU0MDQxNjEsImV4cCI6MTY2NTQxMTM2MX0.h1aCHNqYaRRUL_mkbvSCDx5WHBsTfSpwx9u2-nBb0iY


	if (typeof token !== "undefined") {
		console.log(token)
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return res.send({auth: "failed"})
			} else {
				next()
			}
		})
	} else {
		return res.send({auth: "failed"});
	}
};

//Token Decryption
module.exports.decode = (token) => {
	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload //payload, signature, header
			}
		})
	} else {
		return null
	};
};