const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	enrollments: [
		{
			courseId: {
			type: String,
			required: [true, "Course is required"]
			},
			enrolledOn: {
			type: Date,
			default: new Date()
			},
			status: {
			type: String,
			default: "Enrolled"
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);